# Argus Deployment

## Welke automatisatie?

De ansible scripts die zich in de `deployment` directory bevind, zorgen voor
een correcte deployment van ons security project Argus.

Dit ansible script deployt automatisch 2 of 3 helm charts op basis van een
configuratie die kan meegegeven worden. Er zijn 2 scripts `setup.yml` en
`deploy.yml`, het eerste maakt de Kubernetes omgeving klaar voor een Argus
deployment en de tweede deployt Argus.

## Hoe te gebruiken?

Deze documentatie komt van de wiki pagina [Getting Started](https://github.com/WatcherWhale/Argus/wiki/Getting-Started).

### Prerequisites

Before starting, there are a few prerequisites that need to be present on the device responsible for the rollout of the architecture.
It is assumed you have a working Kubernetes cluster, this also means you need a correctly configured kubectl to access this cluster.
A working Ansible environment should also be available, this is necessary for the automated rollout of all the architecture (in development, version 2.12.5 was used).<br>
Lastly, there has to be a load balancing solution present and a nginx ingress installed.

<br>

### Installation with Ansible

#### Ansible collections

Install the required Ansible collections:

```console
$ ansible-galaxy install -r requirements.yml
```

#### Configuration

First, before deployment, we recommend configuring your setup (can be changed after deployment).
This is done in the `config.yml` in the `deployment` directory. This configuration and its possible values is further explained in the [helm chart documentation](https://github.com/WatcherWhale/SecProA/tree/master/charts/argus).

#### Deploy

##### Initial setup

This command will set up the initial requirements of Argus. These requirements include deploying the custom pod autoscaler and deploying the Kubernetes Argus namespace.

```console
$ ansible-playbook setup.yml
```

It is important to verify if everything is up and running, check if the following commands render a similar output:

```console
$ kubectl get namespace argus
$ kubectl get pods -n kube-system -l name=custom-pod-autoscaler-operator
```

```
NAME   STATUS   AGE
argus  Active   30s

NAME                                             READY   STATUS    RESTARTS   AGE
custom-pod-autoscaler-operator-5b97456c4-9kpjh   1/1     Running   0          30s
```

##### Deploy Argus

Now, we can fully deploy Argus:

```console
$ ansible-playbook deploy.yml
```

As indicated earlier, it is very important to verify everything is up and running:

```console
$ kubectl get all -n argus
```

```
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
argus-selenium-chrome     1/1     1            1           2m28s
argus-selenium-hub        1/1     1            1           2m28s
cookie-checklist          1/1     1            1           2m28s
descriptions              1/1     1            1           2m28s
dns-checklist             1/1     1            1           2m28s
gateway                   1/1     1            1           2m28s
headers-checklist         1/1     1            1           2m28s
https-checklist           1/1     1            1           2m28s
ip-checklist              1/1     1            1           2m28s
mail-checklist            1/1     1            1           2m28s
metrics                   1/1     1            1           2m28s
sequencer                 1/1     1            1           2m28s
vulnerability-checklist   1/1     1            1           2m28s

NAME               READY   AGE
argus-redis-node   3/3     2m28s

NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
argus-redis            ClusterIP   10.96.241.102    <none>        6379/TCP,26379/TCP           2m29s
argus-redis-headless   ClusterIP   None             <none>        6379/TCP,26379/TCP           2m29s
descriptions-server    ClusterIP   10.97.101.11     <none>        3000/TCP                     2m29s
gateway-server         ClusterIP   10.102.47.135    <none>        3000/TCP                     2m29s
metrics-server         ClusterIP   10.98.7.188      <none>        3000/TCP                     2m29s
selenium-hub           ClusterIP   10.105.254.218   <none>        4444/TCP,4443/TCP,4442/TCP   2m29s
sequencer-server       ClusterIP   10.105.146.123   <none>        3000/TCP                     2m29s

NAME            CLASS   HOSTS         ADDRESS       PORTS   AGE
argus-ingress   nginx   argus.local   10.0.30.100   80      2m28s
```


## Wie heeft wat gedaan?

Ik heb praktisch alle logica in ansible geschreven. Deze zijn nagekeken (d.m.v.
Pull Request) door Cato.  De andere team leden hebben hier en daar in de config
waardes aangepast, om zo hun checklists automatisch mee te laten deployen.

