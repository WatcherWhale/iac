#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: argus_push

short_description: Push a request to Arus

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
author:
    - Mathias Maes
'''

from ansible.module_utils.basic import AnsibleModule

import json
import requests
from urllib.parse import urljoin

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        targets=dict(type='dict', required=True),
        url=dict(type='str', required=True),
        tags=dict(type='list', required=False, default=[]),
        checklists=dict(type='list', required=False, default=[])
    )

    result = dict(
        changed=False,
        tracker=""
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    try:
        res = requests.post( urljoin(module.params["url"], "/api/v1/checks/request") , json={
            "targets": module.params["targets"],
            "tags": module.params["tags"],
            "checklists": module.params["checklists"]
        })


        try:
            data = res.json()
        except:
            data = None

        if res.status_code == 201:
            result['changed'] = True
            result['tracker'] = data["tracker"]
        elif data is not None and "message" in data:
            module.fail_json(msg='Request failed: ' + data["message"], **result)
        else:
            module.fail_json(msg='Request failed: Status Code ' + str(res.status_code), **result)

        module.exit_json(**result)
    except Exception as e:
        module.fail_json(msg="Can't connect to API endpoint.", changed=False, error=e)


def main():
    run_module()


if __name__ == '__main__':
    main()
