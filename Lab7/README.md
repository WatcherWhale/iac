# Install

```shell
$ pip install ansible
$ ansible-galaxy collection install community.docker
```

# Configure host

In `hosts.yml` change the `ansible_host` ip adress to the ip adress of your server.

# Deploy

```shell
$ ansible-playbook playbook.yml
```
