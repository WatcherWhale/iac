# Verslag ESXI

Mathias Maes

## Hoe te deployen

Pas de `config.yml` file aan met alle nodige informatie, daarna run `ansible-playbook playbook.yml`.

## Config

| Key | Descriptie |
| --- | ---------- |
| `esxi_hostname` | Ip of DNS Hostname van de ESXI host |
| `esxi_username` | De user waar je alles mee opzet |
| `esxi_password` | Het wachtwoord van deze user |
| `user_username` | De username van een nieuwe user |
| `user_password` | Het wachtwoord van deze user |
| `ova_path` | Plaats op de localhost waar je OVA staat die je wilt deployen. |
